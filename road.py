'''
Project 2
Traffic Problem
Road class
'''

import car
import numpy as np
import bisect

class Road():        
	def __init__(self, length_of_road = 100):
		# Dimensions of road (0,100)
		self.length_of_road = length_of_road

		self.num_of_cars = 0
		self.car_list = [] # car furthest along on the road is the first element in the list

		# Deer attributes
		self.deer = 0
		self.i = 0
		self.deer_positions = [] # register positions of deer appear for animation

	def enter(self, car):
		self.car_list += [car]
		self.num_of_cars += 1


	def insert_deer(self):
		self.deer = 1 # Deer on road = True

		# Randomly insert a deer at some point along the road
		deer_pos = np.random.random() * 100
		self.deer_positions += [[deer_pos, 50]] # second parameter is the frames to be shown in animation 

		# Make a list of all the cars positions along the road
		car_pos_list = []
		for i in self.car_list:
			car_pos_list += [i.pos]

		# Find appropriate index based on deer_pos and car_pos_list
		self.i = len(car_pos_list) - bisect.bisect_right(car_pos_list[::-1], deer_pos)

		# Message for deer
		print "Deer at %f, inserted at index %d" %(deer_pos, self.i)

		# Create deer and insert into car_list
		deer_object = car.Car(pos = deer_pos, vel = 0)
		self.car_list.insert(self.i, deer_object)


	def exit(self, car):
		self.car_list = self.car_list[1:]
		self.num_of_cars -= 1
		
	def check_exit(self):
		# This should be called after every time step.

		# If there is a deer, remove it
		if (self.deer): # 0/1 are boolean values
			self.car_list.pop(self.i)
			self.deer = 0

		# Check to see if we have any cars
		# that have passed the end of the road (aka car.pos > 100) and remove them via exit.
		if self.car_list == []: # This avoids our problem of having no cars on our road
			return
		elif self.car_list[0].pos > 100:
			self.exit(self.car_list[0])
			return self.check_exit()


