
import numpy as np
from main import time_step
from road import *
from car import *
import matplotlib
from matplotlib import pyplot as plt
import sys
from scipy import optimize


num_simulations=300                                #number of runs

time=np.empty(num_simulations)                     #our data arrays
num_deer=np.empty(num_simulations)

for i in range(num_simulations):
    print np.float(i)/num_simulations
    Turnpike=Road()
    for t in range(0,3000):                        #run begins
        time_step(Turnpike,t)
        if t>2 and Turnpike.car_list[-1].pos>98.9: #condition for the end of each run
            break
    time[i]=t-50                                   #append data for this run to our array
    num_deer[i]=Turnpike.num_of_deer

np.save("sim5_data.npy",[num_deer,time])           #saves data


#this loads arrays from previously run simulations; whence begins the plotting section

[num_deer,time]=np.load("sim_data.npy")
num_deer=np.append(num_deer,np.load("sim1_data.npy")[0])
num_deer=np.append(num_deer,np.load("sim2_data.npy")[0])
num_deer=np.append(num_deer,np.load("sim3_data.npy")[0]) 
num_deer=np.append(num_deer,np.load("sim4_data.npy")[0]) 
num_deer=np.append(num_deer,np.load("sim5_data.npy")[0]) 
time=np.append(time,np.load("sim1_data.npy")[1])
time=np.append(time,np.load("sim2_data.npy")[1])
time=np.append(time,np.load("sim3_data.npy")[1])
time=np.append(time,np.load("sim4_data.npy")[1])
time=np.append(time,np.load("sim5_data.npy")[1]) 


#Perform a fit and plot results

lin_fit=0
other_fit=1

numbers=np.arange(1200)/10.

if (lin_fit):
    linfit=np.polyfit(num_deer,time,1)
    #print linfit[0]
    plt.plot(num_deer, time, 'b.', numbers, linfit[0]*numbers+linfit[1], 'r')
elif (other_fit):
    def f(x,a,b):
        return 1100+a*x**b
    fit=optimize.curve_fit(f,num_deer,time)
    print fit
    plt.plot(num_deer, time, 'b.', numbers, 1100+fit[0][0]*numbers**fit[0][1], 'r')
else:
    plt.plot(num_deer, time, 'b.')

 

plt.xlim([0,120])
plt.ylim([1100,1450])
plt.title('Time for Last Car to Finish as a Function of # Deer')
plt.xlabel('Number of deer')
plt.ylabel('Time for Last Car to Finish [Simulation Time]')


#plt.savefig('Figure5')
plt.show()

